import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

public class GrafikFungsi{
	public static void main (String [] args){
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
		
		String pilihperulangan = null;
	    int perulangan = 0;
		do{
		System.out.println("Program mencari letak grafik fungsi pada bidang kartesius");
		System.out.println("inputannya dari file bereksistensi .txt");
		System.out.println("f(x) = ax^2 + bx + c");
		
		System.out.println("Masukkan lokasi file nilai a");
		System.out.println("Dengan format = direktori:/namafolderlokasi/namafile.eksistensifile");
		System.out.print("Lokasi file nilai a = ");
		String inputLokasiA =null;
		try{
			inputLokasiA = bufferedreader.readLine();
		}
		catch(IOException error){
			System.out.println("Input Error"+error.getMessage());
		}
		
		System.out.println("Masukkan lokasi file nilai b");
		System.out.println("Dengan format = direktori:/namafolderlokasi/namafile.eksistensifile");
		System.out.print("Lokasi file nilai b = ");
		String inputLokasiB =null;
		try{
			inputLokasiB = bufferedreader.readLine();
		}
		catch(IOException error){
			System.out.println("Input Error"+error.getMessage());
		}
		
		System.out.println("Masukkan lokasi file nilai c");
		System.out.println("Dengan format = direktori:/namafolderlokasi/namafile.eksistensifile");
		System.out.print("Lokasi file nilai c = ");
		String inputLokasiC =null;
		try{
			inputLokasiC = bufferedreader.readLine();
		}
		catch(IOException error){
			System.out.println("Input Error"+error.getMessage());
		}
		
		try{
		File lokasiA = new File(inputLokasiA);
		File lokasiB = new File(inputLokasiB);
		File lokasiC = new File(inputLokasiC);
		FileReader bacaFileA = new FileReader(lokasiA);
		FileReader bacaFileB = new FileReader(lokasiB);
		FileReader bacaFileC = new FileReader(lokasiC);
		BufferedReader pembacaFileA = new BufferedReader(bacaFileA);
		BufferedReader pembacaFileB = new BufferedReader(bacaFileB);
		BufferedReader pembacaFileC = new BufferedReader(bacaFileC);
		StringBuffer bacaIsiFileA = new StringBuffer();
		StringBuffer bacaIsiFileB = new StringBuffer();
		StringBuffer bacaIsiFileC = new StringBuffer();
		String isiFileA = null;
		String isiFileB = null;
		String isiFileC = null;
		
		while ((isiFileA = pembacaFileA.readLine()) != null && (isiFileB = pembacaFileB.readLine()) != null && (isiFileC = pembacaFileC.readLine())!= null){
		int nilaiA = Integer.parseInt(isiFileA);
		int nilaiB = Integer.parseInt(isiFileB);
		int nilaiC = Integer.parseInt(isiFileC);
		
		System.out.println();
		System.out.println("f(x) = " + nilaiA + "x^2 + " + nilaiB + "x + " + nilaiC);
		String grafik = "f(x) = " + nilaiA + "x^2 + " + nilaiB + "x + " + nilaiC;
		
		
		if(nilaiA > 0){
			System.out.println("Grafik terbuka keatas, Titik balik minimum");
		}
		else{
			System.out.println("Grafik terbuka kebawah, Titik balik maksimum");
		}
		
		if(nilaiC > 0){
			System.out.println("Grafik memotong sumbu Y diatas sumbu X");
		}
		else if(nilaiC < 0){
			System.out.println("Grafik memotong sumbu Y dibawah sumbu X");
		}
		else{
			System.out.println("Grafik melalui titik (0,0)");
		}
		
		if(nilaiA*nilaiB > 0){
			System.out.println("Titik Balik terletak dikanan sumbu Y");
		}
		else if(nilaiA*nilaiB < 0){
			System.out.println("Titik Balik terletak dikiri sumbu Y");
		}
		else{
			System.out.println("Titik Balik terletak di sumbu Y");
		}
		
		int x = -(nilaiB/(2*nilaiA));
		double z = 4*nilaiA*nilaiC;
		double D = Math.pow(nilaiB,2) + z;
		double y = -(D/(4*nilaiA));
		System.out.println("Titik balik ("+ x + "," + y + ")");
		}
		
		}
		catch (Exception e){
			e.printStackTrace();
		}
	
	System.out.println("");
	System.out.println("Apakah Anda ingin mengulangi proses?");
	        System.out.println("1. yes 2. no");
			System.out.print("Masukkan pilihan anda = ");
	        try {
                pilihperulangan= bufferedreader.readLine();
                try  {
                    perulangan = Integer.parseInt(pilihperulangan);
                    if (perulangan ==1) {
                    	perulangan = 1;
                    }
                    else if (perulangan == 2){
                    	perulangan = -1;
                    }
                    else
                    	System.out.println("Anda salah memasukkan pilihan");
                }
                catch(NumberFormatException e){
                	System.out.println("Anda salah memasukkan format data");
                }
            }
	        catch(IOException error){
            	System.out.println("salah memasukkan pilihan"+error.getMessage());
	        } 
		} while(perulangan==1);
	}
}