import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;

public class PermutasiDanKombinasi{
	static void permutasi(){
		
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Masukkan lokasi file nilai n");
		System.out.println("Dengan format = direktori:/namafolderlokasi/namafile.eksistensifile");
		System.out.print("Lokasi file nilai n = ");
		String inputLokasiN =null;
		try{
			inputLokasiN = bufferedreader.readLine();
		}
		catch(IOException error){
			System.out.println("Input Error"+error.getMessage());
		}
		
		System.out.println("Masukkan lokasi file nilai r");
		System.out.println("Dengan format = direktori:/namafolderlokasi/namafile.eksistensifile");
		System.out.print("Lokasi file nilai r = ");
		String inputLokasiR =null;
		try{
			inputLokasiR = bufferedreader.readLine();
		}
		catch(IOException error){
			System.out.println("Input Error"+error.getMessage());
		}
		
		try{
			File lokasiN = new File(inputLokasiN);
			File lokasiR = new File(inputLokasiR);
			FileReader bacaFileN = new FileReader (lokasiN);
			FileReader bacaFileR = new FileReader (lokasiR);
			BufferedReader pembacaFileN = new BufferedReader(bacaFileN);
			BufferedReader pembacaFileR = new BufferedReader(bacaFileR);
			StringBuffer bacaIsiFileN = new StringBuffer();
			StringBuffer bacaIsiFileR = new StringBuffer();
  			String isiFileN = null;
			String isiFileR = null;
			
			while ((isiFileN = pembacaFileN.readLine()) != null && (isiFileR = pembacaFileR.readLine()) != null) {
				int nilaiN = Integer.parseInt(isiFileN);
				int nilaiR = Integer.parseInt(isiFileR);
				
				System.out.println("Nilai n = "+nilaiN);
				System.out.println("Nilai r = "+nilaiR);				
				if(nilaiN > nilaiR && nilaiN>0 && nilaiR>0){
					int faktorialN = 1, faktorialPengurangan = 1, pengurangan = nilaiN - nilaiR;
					
					for(int indeks = 1; indeks<=nilaiN; indeks++){
						faktorialN = faktorialN * indeks;
					}
					
					for(int indeks = 1; indeks<=pengurangan; indeks++){
						faktorialPengurangan = faktorialPengurangan * indeks;
					}
					int permutasi = faktorialN/faktorialPengurangan;
					System.out.println("Hasil Permutasi = " + permutasi);
				}
				else{
					System.out.println("Nilai tidak dapat dihitung");
				}
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
	
	static void kombinasi(){
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Masukkan lokasi file nilai n");
		System.out.println("Dengan format = direktori:/namafolderloaksi/namafile.eksistensifile");
		System.out.print("Lokasi file nilai n = ");
		String inputLokasiN =null;
		try{
			inputLokasiN = bufferedreader.readLine();
		}
		catch(IOException error){
			System.out.println("Input Error"+error.getMessage());
		}
		
		System.out.println("Masukkan lokasi file nilai r");
		System.out.println("Dengan format = direktori:/namafolderlokasi/namafile.eksistensifile");
		System.out.print("Lokasi file nilai r = ");
		String inputLokasiR =null;
		try{
			inputLokasiR = bufferedreader.readLine();
		}
		catch(IOException error){
			System.out.println("Input Error"+error.getMessage());
		}
		
		try{
			File lokasiN = new File(inputLokasiN);
			File lokasiR = new File(inputLokasiR);
			FileReader bacaFileN = new FileReader (lokasiN);
			FileReader bacaFileR = new FileReader (lokasiR);
			BufferedReader pembacaFileN = new BufferedReader(bacaFileN);
			BufferedReader pembacaFileR = new BufferedReader(bacaFileR);
			StringBuffer bacaIsiFileN = new StringBuffer();
			StringBuffer bacaIsiFileR = new StringBuffer();
  			String isiFileN = null;
			String isiFileR = null;
			
			while ((isiFileN = pembacaFileN.readLine()) != null && (isiFileR = pembacaFileR.readLine()) != null) {
				int nilaiN = Integer.parseInt(isiFileN);
				int nilaiR = Integer.parseInt(isiFileR);
				
				System.out.println("Nilai n = "+nilaiN);
				System.out.println("Nilai r = "+nilaiR);
				
				if(nilaiN > nilaiR && nilaiN>0 && nilaiR>0){
					int faktorialN = 1, faktorialPengurangan = 1, faktorialR = 1, pengurangan = nilaiN - nilaiR;
					
					for(int indeks = 1; indeks<=nilaiN; indeks++){
						faktorialN = faktorialN * indeks;
					}
					
					for(int indeks = 1; indeks<=pengurangan; indeks++){
						faktorialPengurangan = faktorialPengurangan * indeks;
					}
					
					for(int indeks = 1; indeks<=nilaiR; indeks++){
						faktorialR = faktorialR * indeks;
					}
					
					int kombinasi = faktorialN/(faktorialR * faktorialPengurangan);
					System.out.println("Hasil Kombinasi = " + kombinasi);
				}
				else{
					System.out.println("Nilai tidak dapat dihitung");
				}
			}
		}
		catch (Exception e){
			e.printStackTrace();
		}
	}
	
	public static void main(String []args){
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
		
		String pilihperulangan = null;
	    int perulangan = 0;
		do{
		System.out.println("Program untuk mencari nilai Permutasi dan Kombinasi yang inputannya berupa file .txt dan outputnya di file .txt");
		System.out.println("Menu Pilihan");
		System.out.println("1. Kombinasi");
		System.out.println("2. Permutasi");
		System.out.println("0. Keluar");
		System.out.print("Masukkan Pilihan Anda");
		System.out.print("Pilihan Anda = ");
		String inputpilihan = null;
		try{
			inputpilihan = bufferedreader.readLine();
		}
		catch (IOException error){
			System.out.println("Input Error"+ error.getMessage());
		}
		
		try{
			int pilihan = Integer.parseInt(inputpilihan);
			switch(pilihan){
				case 0:
					break;
				case 1:
					kombinasi();
					break;
				case 2:
					permutasi();
					break;
				default:
					System.out.println("Anda salah memasukkan pilihan");
					break;
			}
		}
		catch (NumberFormatException e){
			System.out.println("Anda salah memasukkan format data");
		}
		
		System.out.println("Apakah Anda ingin mengulangi proses?");
	        System.out.println("1. yes 2. no");
			System.out.print("Masukkan pilihan anda = ");
	        try {
                pilihperulangan= bufferedreader.readLine();
                try  {
                    perulangan = Integer.parseInt(pilihperulangan);
                    if (perulangan ==1) {
                    	perulangan = 1;
                    }
                    else if (perulangan == 2){
                    	perulangan = -1;
                    }
                    else
                    	System.out.println("Anda salah memasukkan pilihan");
                }
                catch(NumberFormatException e){
                	System.out.println("Anda salah memasukkan format data");
                }
            }
	        catch(IOException error){
            	System.out.println("salah memasukkan pilihan"+error.getMessage());
	        } 
		} while(perulangan==1);
		
	}
}