import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Scanner;

public class TugasCutPaste{
	public static void main (String[]args){
		
		InputStream inputFile = null;
		OutputStream outputFile = null;
		Scanner input = new Scanner (System.in);
		
		System.out.println("Program untuk mengcut file bereksistensi .txt");
		
			try{
			System.out.println("Silahkan masukkan asal file");
			System.out.println("Dengan format = direktori:/namafolderasal/namafile.eksistensifile");
				System.out.print("Asal File = ");
				String fileasal = input.nextLine();
				File filepotong = new File(fileasal);
				
				try{
				System.out.println("Silahkan masukkan tujuan file");
				System.out.println("Dengan format = direktori:/namafoldertujuan/namafile.eksistensifile");
				System.out.print("Tujuan File = ");
				String filetujuan = input.nextLine();
				File filetempel = new File(filetujuan);
				inputFile = new FileInputStream(filepotong);
				outputFile = new FileOutputStream(filetempel);
			
					byte [] bataspanjangfile = new byte[2048];
					int panjangfile;
			
					while ((panjangfile= inputFile.read(bataspanjangfile))>=0){
					outputFile.write(panjangfile);
					}
			
					inputFile.close();
					outputFile.close();
					filepotong.delete();
			
					System.out.println("Sukses mengcut file");
				}
				catch(Exception e){
				System.out.println("Lokasi tujuan file yang anda masukkan tidak ditemukan");
				}
			}
			catch(Exception e){
				System.out.println("Lokasi asal file yang anda masukkan tidak ditemukan");
			}
	}
}
