import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.io.IOException;

public class SegitigaPascal{
	public static void main (String[]args){
		BufferedReader bufferedreader = new BufferedReader(new InputStreamReader(System.in));
		
		System.out.println("Program Membuat segitiga Pascal yang inputan dan outputnya merupakan file bereksistensi .txt");
		System.out.print("Masukkan Tinggi Segitiga Pascal pada file = ");
		String inputIsiFile = null;
		try{
			inputIsiFile = bufferedreader.readLine();
		}
		catch(IOException e){
			System.out.println("Input Error"+e.getMessage());
		}
		
		String isiFile = inputIsiFile;
		System.out.println("Masukkan lokasi file = ");
		try{			
		String inputLokasiFile = null;		
			try{
				inputLokasiFile = bufferedreader.readLine();
			}
			catch(IOException error){
				System.out.println("Input Error"+error.getMessage());
			}
		String lokasiFile = inputLokasiFile;
		File lokasiHasil = new File (lokasiFile);
		FileWriter tulisFile = new FileWriter(lokasiHasil.getAbsoluteFile());
		BufferedWriter bufferedwriter = new BufferedWriter(tulisFile);
		bufferedwriter.write("Tinggi Segitiga Pascal = " + isiFile);
		bufferedwriter.close();
		}
		
		catch(Exception e){
			e.printStackTrace();
		}

			int tinggi = Integer.parseInt(isiFile);
			int segitiga[][] = new int[tinggi][tinggi];
			
			for(int indeksbaris=0; indeksbaris<tinggi; indeksbaris++){
				for(int indekskolom=0; indekskolom<tinggi; indekskolom++){
					if((indeksbaris+indekskolom)>=(tinggi-1)){
						if((indeksbaris+indekskolom)==(tinggi-1)||(indekskolom ==(tinggi-1))){
							segitiga[indeksbaris][indekskolom]=1;
						}
						else{
						segitiga[indeksbaris][indekskolom]=((segitiga[indeksbaris-1][indekskolom])+(segitiga[indeksbaris-1][indekskolom+1]));
						}
					System.out.print(segitiga[indeksbaris][indekskolom] + "   ");
					}
					else{
						System.out.print("  ");
					}
				}
			System.out.println(" ");
			}
		}
	}